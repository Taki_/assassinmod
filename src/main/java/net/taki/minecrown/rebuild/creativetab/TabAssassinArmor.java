
package net.taki.minecrown.rebuild.creativetab;

@ElementsAssassincrownMod.ModElement.Tag
public class TabAssassinArmor extends ElementsAssassincrownMod.ModElement {

	public TabAssassinArmor(ElementsAssassincrownMod instance) {
		super(instance, 6);
	}

	@Override
	public void initElements() {
		tab = new CreativeTabs("tabassassin_armor") {
			@SideOnly(Side.CLIENT)
			@Override
			public ItemStack getTabIconItem() {
				return new ItemStack(ItemAltair.helmet, (int) (1));
			}

			@SideOnly(Side.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
	}

	public static CreativeTabs tab;

}
