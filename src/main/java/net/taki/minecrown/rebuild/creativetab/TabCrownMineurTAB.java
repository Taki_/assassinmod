
package net.taki.minecrown.rebuild.creativetab;

import net.taki.minecrown.rebuild.ElementsAssassincrownMod;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;

import net.minecraft.item.ItemStack;
import net.minecraft.init.Items;
import net.minecraft.creativetab.CreativeTabs;

@ElementsAssassincrownMod.ModElement.Tag
public class TabCrownMineurTAB extends ElementsAssassincrownMod.ModElement {
	public TabCrownMineurTAB(ElementsAssassincrownMod instance) {
		super(instance, 28);
	}

	@Override
	public void initElements() {
		tab = new CreativeTabs("tabcrown_mineur_tab") {
			@SideOnly(Side.CLIENT)
			@Override
			public ItemStack getTabIconItem() {
				return new ItemStack(Items.DIAMOND, (int) (1));
			}

			@SideOnly(Side.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
	}
	public static CreativeTabs tab;
}
