
package net.taki.minecrown.rebuild.creativetab;

@ElementsAssassincrownMod.ModElement.Tag
public class TabAssassinWeapons extends ElementsAssassincrownMod.ModElement {

	public TabAssassinWeapons(ElementsAssassincrownMod instance) {
		super(instance, 1);
	}

	@Override
	public void initElements() {
		tab = new CreativeTabs("tabassassin_weapons") {
			@SideOnly(Side.CLIENT)
			@Override
			public ItemStack getTabIconItem() {
				return new ItemStack(ItemHiddenBladeA.block, (int) (1));
			}

			@SideOnly(Side.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
	}

	public static CreativeTabs tab;

}
