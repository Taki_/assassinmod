
package net.taki.minecrown.rebuild.creativetab;

import net.taki.minecrown.rebuild.ElementsAssassincrownMod;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;

import net.minecraft.item.ItemStack;
import net.minecraft.init.Blocks;
import net.minecraft.creativetab.CreativeTabs;

@ElementsAssassincrownMod.ModElement.Tag
public class TabJobsTabBotaniste extends ElementsAssassincrownMod.ModElement {
	public TabJobsTabBotaniste(ElementsAssassincrownMod instance) {
		super(instance, 36);
	}

	@Override
	public void initElements() {
		tab = new CreativeTabs("tabjobs_tab_botaniste") {
			@SideOnly(Side.CLIENT)
			@Override
			public ItemStack getTabIconItem() {
				return new ItemStack(Blocks.RED_FLOWER, (int) (1), 5);
			}

			@SideOnly(Side.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
	}
	public static CreativeTabs tab;
}
