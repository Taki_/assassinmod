
package net.taki.minecrown.rebuild.creativetab;

import net.taki.minecrown.rebuild.ElementsAssassincrownMod;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;

import net.minecraft.item.ItemStack;
import net.minecraft.init.Items;
import net.minecraft.creativetab.CreativeTabs;

@ElementsAssassincrownMod.ModElement.Tag
public class TabJobsForge extends ElementsAssassincrownMod.ModElement {
	public TabJobsForge(ElementsAssassincrownMod instance) {
		super(instance, 33);
	}

	@Override
	public void initElements() {
		tab = new CreativeTabs("tabjobs_forge") {
			@SideOnly(Side.CLIENT)
			@Override
			public ItemStack getTabIconItem() {
				return new ItemStack(Items.DIAMOND_SWORD, (int) (1));
			}

			@SideOnly(Side.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
	}
	public static CreativeTabs tab;
}
