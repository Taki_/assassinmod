
package net.taki.minecrown.rebuild.item;

@ElementsAssassincrownMod.ModElement.Tag
public class ItemEzio extends ElementsAssassincrownMod.ModElement {

	@GameRegistry.ObjectHolder("assassincrown:eziohelmet")
	public static final Item helmet = null;

	@GameRegistry.ObjectHolder("assassincrown:eziobody")
	public static final Item body = null;

	@GameRegistry.ObjectHolder("assassincrown:eziolegs")
	public static final Item legs = null;

	@GameRegistry.ObjectHolder("assassincrown:ezioboots")
	public static final Item boots = null;

	public ItemEzio(ElementsAssassincrownMod instance) {
		super(instance, 11);
	}

	@Override
	public void initElements() {
		ItemArmor.ArmorMaterial enuma = EnumHelper.addArmorMaterial("EZIO", "assassincrown:ezio__overlay", 25, new int[]{2, 5, 6, 2}, 9,
				(net.minecraft.util.SoundEvent) net.minecraft.util.SoundEvent.REGISTRY.getObject(new ResourceLocation("")), 0f);

		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.HEAD).setUnlocalizedName("eziohelmet").setRegistryName("eziohelmet")
				.setCreativeTab(TabAssassinArmor.tab));

		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.CHEST).setUnlocalizedName("eziobody").setRegistryName("eziobody")
				.setCreativeTab(TabAssassinArmor.tab));

		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.LEGS).setUnlocalizedName("eziolegs").setRegistryName("eziolegs")
				.setCreativeTab(TabAssassinArmor.tab));

		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.FEET).setUnlocalizedName("ezioboots").setRegistryName("ezioboots")
				.setCreativeTab(TabAssassinArmor.tab));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(helmet, 0, new ModelResourceLocation("assassincrown:eziohelmet", "inventory"));
		ModelLoader.setCustomModelResourceLocation(body, 0, new ModelResourceLocation("assassincrown:eziobody", "inventory"));
		ModelLoader.setCustomModelResourceLocation(legs, 0, new ModelResourceLocation("assassincrown:eziolegs", "inventory"));
		ModelLoader.setCustomModelResourceLocation(boots, 0, new ModelResourceLocation("assassincrown:ezioboots", "inventory"));
	}

}
