
package net.taki.minecrown.rebuild.item;

@ElementsAssassincrownMod.ModElement.Tag
public class ItemShay extends ElementsAssassincrownMod.ModElement {

	@GameRegistry.ObjectHolder("assassincrown:shayhelmet")
	public static final Item helmet = null;

	@GameRegistry.ObjectHolder("assassincrown:shaybody")
	public static final Item body = null;

	@GameRegistry.ObjectHolder("assassincrown:shaylegs")
	public static final Item legs = null;

	@GameRegistry.ObjectHolder("assassincrown:shayboots")
	public static final Item boots = null;

	public ItemShay(ElementsAssassincrownMod instance) {
		super(instance, 14);
	}

	@Override
	public void initElements() {
		ItemArmor.ArmorMaterial enuma = EnumHelper.addArmorMaterial("SHAY", "assassincrown:shaycormac__overlay", 25, new int[]{2, 5, 6, 2}, 9,
				(net.minecraft.util.SoundEvent) net.minecraft.util.SoundEvent.REGISTRY.getObject(new ResourceLocation("")), 0f);

		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.HEAD).setUnlocalizedName("shayhelmet").setRegistryName("shayhelmet")
				.setCreativeTab(TabAssassinArmor.tab));

		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.CHEST).setUnlocalizedName("shaybody").setRegistryName("shaybody")
				.setCreativeTab(TabAssassinArmor.tab));

		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.LEGS).setUnlocalizedName("shaylegs").setRegistryName("shaylegs")
				.setCreativeTab(TabAssassinArmor.tab));

		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.FEET).setUnlocalizedName("shayboots").setRegistryName("shayboots")
				.setCreativeTab(TabAssassinArmor.tab));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(helmet, 0, new ModelResourceLocation("assassincrown:shayhelmet", "inventory"));
		ModelLoader.setCustomModelResourceLocation(body, 0, new ModelResourceLocation("assassincrown:shaybody", "inventory"));
		ModelLoader.setCustomModelResourceLocation(legs, 0, new ModelResourceLocation("assassincrown:shaylegs", "inventory"));
		ModelLoader.setCustomModelResourceLocation(boots, 0, new ModelResourceLocation("assassincrown:shayboots", "inventory"));
	}

}
