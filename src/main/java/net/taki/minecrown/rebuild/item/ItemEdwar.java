
package net.taki.minecrown.rebuild.item;

@ElementsAssassincrownMod.ModElement.Tag
public class ItemEdwar extends ElementsAssassincrownMod.ModElement {

	@GameRegistry.ObjectHolder("assassincrown:edwarhelmet")
	public static final Item helmet = null;

	@GameRegistry.ObjectHolder("assassincrown:edwarbody")
	public static final Item body = null;

	@GameRegistry.ObjectHolder("assassincrown:edwarlegs")
	public static final Item legs = null;

	@GameRegistry.ObjectHolder("assassincrown:edwarboots")
	public static final Item boots = null;

	public ItemEdwar(ElementsAssassincrownMod instance) {
		super(instance, 10);
	}

	@Override
	public void initElements() {
		ItemArmor.ArmorMaterial enuma = EnumHelper.addArmorMaterial("EDWAR", "assassincrown:edwardkenway__overlay", 25, new int[]{2, 5, 6, 2}, 9,
				(net.minecraft.util.SoundEvent) net.minecraft.util.SoundEvent.REGISTRY.getObject(new ResourceLocation("")), 0f);

		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.HEAD).setUnlocalizedName("edwarhelmet").setRegistryName("edwarhelmet")
				.setCreativeTab(TabAssassinArmor.tab));

		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.CHEST).setUnlocalizedName("edwarbody").setRegistryName("edwarbody")
				.setCreativeTab(TabAssassinArmor.tab));

		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.LEGS).setUnlocalizedName("edwarlegs").setRegistryName("edwarlegs")
				.setCreativeTab(TabAssassinArmor.tab));

		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.FEET).setUnlocalizedName("edwarboots").setRegistryName("edwarboots")
				.setCreativeTab(TabAssassinArmor.tab));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(helmet, 0, new ModelResourceLocation("assassincrown:edwarhelmet", "inventory"));
		ModelLoader.setCustomModelResourceLocation(body, 0, new ModelResourceLocation("assassincrown:edwarbody", "inventory"));
		ModelLoader.setCustomModelResourceLocation(legs, 0, new ModelResourceLocation("assassincrown:edwarlegs", "inventory"));
		ModelLoader.setCustomModelResourceLocation(boots, 0, new ModelResourceLocation("assassincrown:edwarboots", "inventory"));
	}

}
