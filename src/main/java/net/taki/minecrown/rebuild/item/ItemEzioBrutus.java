
package net.taki.minecrown.rebuild.item;

@ElementsAssassincrownMod.ModElement.Tag
public class ItemEzioBrutus extends ElementsAssassincrownMod.ModElement {

	@GameRegistry.ObjectHolder("assassincrown:ezio_brutushelmet")
	public static final Item helmet = null;

	@GameRegistry.ObjectHolder("assassincrown:ezio_brutusbody")
	public static final Item body = null;

	@GameRegistry.ObjectHolder("assassincrown:ezio_brutuslegs")
	public static final Item legs = null;

	@GameRegistry.ObjectHolder("assassincrown:ezio_brutusboots")
	public static final Item boots = null;

	public ItemEzioBrutus(ElementsAssassincrownMod instance) {
		super(instance, 12);
	}

	@Override
	public void initElements() {
		ItemArmor.ArmorMaterial enuma = EnumHelper.addArmorMaterial("EZIO_BRUTUS", "assassincrown:eziobrutus__overlay", 25, new int[]{2, 5, 6, 2}, 9,
				(net.minecraft.util.SoundEvent) net.minecraft.util.SoundEvent.REGISTRY.getObject(new ResourceLocation("")), 0f);

		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.HEAD).setUnlocalizedName("ezio_brutushelmet")
				.setRegistryName("ezio_brutushelmet").setCreativeTab(TabAssassinArmor.tab));

		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.CHEST).setUnlocalizedName("ezio_brutusbody")
				.setRegistryName("ezio_brutusbody").setCreativeTab(TabAssassinArmor.tab));

		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.LEGS).setUnlocalizedName("ezio_brutuslegs")
				.setRegistryName("ezio_brutuslegs").setCreativeTab(TabAssassinArmor.tab));

		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.FEET).setUnlocalizedName("ezio_brutusboots")
				.setRegistryName("ezio_brutusboots").setCreativeTab(TabAssassinArmor.tab));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(helmet, 0, new ModelResourceLocation("assassincrown:ezio_brutushelmet", "inventory"));
		ModelLoader.setCustomModelResourceLocation(body, 0, new ModelResourceLocation("assassincrown:ezio_brutusbody", "inventory"));
		ModelLoader.setCustomModelResourceLocation(legs, 0, new ModelResourceLocation("assassincrown:ezio_brutuslegs", "inventory"));
		ModelLoader.setCustomModelResourceLocation(boots, 0, new ModelResourceLocation("assassincrown:ezio_brutusboots", "inventory"));
	}

}
