
package net.taki.minecrown.rebuild.item;

@ElementsAssassincrownMod.ModElement.Tag
public class ItemArno extends ElementsAssassincrownMod.ModElement {

	@GameRegistry.ObjectHolder("assassincrown:arnohelmet")
	public static final Item helmet = null;

	@GameRegistry.ObjectHolder("assassincrown:arnobody")
	public static final Item body = null;

	@GameRegistry.ObjectHolder("assassincrown:arnolegs")
	public static final Item legs = null;

	@GameRegistry.ObjectHolder("assassincrown:arnoboots")
	public static final Item boots = null;

	public ItemArno(ElementsAssassincrownMod instance) {
		super(instance, 8);
	}

	@Override
	public void initElements() {
		ItemArmor.ArmorMaterial enuma = EnumHelper.addArmorMaterial("ARNO", "assassincrown:arnodorian__overlay", 25, new int[]{2, 5, 6, 2}, 9,
				(net.minecraft.util.SoundEvent) net.minecraft.util.SoundEvent.REGISTRY.getObject(new ResourceLocation("")), 0f);

		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.HEAD).setUnlocalizedName("arnohelmet").setRegistryName("arnohelmet")
				.setCreativeTab(TabAssassinArmor.tab));

		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.CHEST).setUnlocalizedName("arnobody").setRegistryName("arnobody")
				.setCreativeTab(TabAssassinArmor.tab));

		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.LEGS).setUnlocalizedName("arnolegs").setRegistryName("arnolegs")
				.setCreativeTab(TabAssassinArmor.tab));

		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.FEET).setUnlocalizedName("arnoboots").setRegistryName("arnoboots")
				.setCreativeTab(TabAssassinArmor.tab));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(helmet, 0, new ModelResourceLocation("assassincrown:arnohelmet", "inventory"));
		ModelLoader.setCustomModelResourceLocation(body, 0, new ModelResourceLocation("assassincrown:arnobody", "inventory"));
		ModelLoader.setCustomModelResourceLocation(legs, 0, new ModelResourceLocation("assassincrown:arnolegs", "inventory"));
		ModelLoader.setCustomModelResourceLocation(boots, 0, new ModelResourceLocation("assassincrown:arnoboots", "inventory"));
	}

}
