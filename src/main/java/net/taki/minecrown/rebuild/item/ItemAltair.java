
package net.taki.minecrown.rebuild.item;

@ElementsAssassincrownMod.ModElement.Tag
public class ItemAltair extends ElementsAssassincrownMod.ModElement {

	@GameRegistry.ObjectHolder("assassincrown:altairhelmet")
	public static final Item helmet = null;

	@GameRegistry.ObjectHolder("assassincrown:altairbody")
	public static final Item body = null;

	@GameRegistry.ObjectHolder("assassincrown:altairlegs")
	public static final Item legs = null;

	@GameRegistry.ObjectHolder("assassincrown:altairboots")
	public static final Item boots = null;

	public ItemAltair(ElementsAssassincrownMod instance) {
		super(instance, 7);
	}

	@Override
	public void initElements() {
		ItemArmor.ArmorMaterial enuma = EnumHelper.addArmorMaterial("ALTAIR", "assassincrown:altair__overlay", 25, new int[]{2, 5, 6, 2}, 9,
				(net.minecraft.util.SoundEvent) net.minecraft.util.SoundEvent.REGISTRY.getObject(new ResourceLocation("")), 0f);

		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.HEAD).setUnlocalizedName("altairhelmet").setRegistryName("altairhelmet")
				.setCreativeTab(TabAssassinArmor.tab));

		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.CHEST).setUnlocalizedName("altairbody").setRegistryName("altairbody")
				.setCreativeTab(TabAssassinArmor.tab));

		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.LEGS).setUnlocalizedName("altairlegs").setRegistryName("altairlegs")
				.setCreativeTab(TabAssassinArmor.tab));

		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.FEET).setUnlocalizedName("altairboots").setRegistryName("altairboots")
				.setCreativeTab(TabAssassinArmor.tab));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(helmet, 0, new ModelResourceLocation("assassincrown:altairhelmet", "inventory"));
		ModelLoader.setCustomModelResourceLocation(body, 0, new ModelResourceLocation("assassincrown:altairbody", "inventory"));
		ModelLoader.setCustomModelResourceLocation(legs, 0, new ModelResourceLocation("assassincrown:altairlegs", "inventory"));
		ModelLoader.setCustomModelResourceLocation(boots, 0, new ModelResourceLocation("assassincrown:altairboots", "inventory"));
	}

}
