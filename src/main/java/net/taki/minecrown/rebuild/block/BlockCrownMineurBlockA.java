
package net.taki.minecrown.rebuild.block;

import net.taki.minecrown.rebuild.creativetab.TabAssassinWeapons;
import net.taki.minecrown.rebuild.ElementsAssassincrownMod;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.event.ModelRegistryEvent;

import net.minecraft.item.ItemBlock;
import net.minecraft.item.Item;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.block.material.Material;
import net.minecraft.block.SoundType;
import net.minecraft.block.Block;

@ElementsAssassincrownMod.ModElement.Tag
public class BlockCrownMineurBlockA extends ElementsAssassincrownMod.ModElement {
	@GameRegistry.ObjectHolder("assassincrown:crown_mineur_block_a")
	public static final Block block = null;
	public BlockCrownMineurBlockA(ElementsAssassincrownMod instance) {
		super(instance, 32);
	}

	@Override
	public void initElements() {
		elements.blocks.add(() -> new BlockCustom().setRegistryName("crown_mineur_block_a"));
		elements.items.add(() -> new ItemBlock(block).setRegistryName(block.getRegistryName()));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(block), 0,
				new ModelResourceLocation("assassincrown:crown_mineur_block_a", "inventory"));
	}
	public static class BlockCustom extends Block {
		public BlockCustom() {
			super(Material.IRON);
			setUnlocalizedName("crown_mineur_block_a");
			setSoundType(SoundType.STONE);
			setHardness(1F);
			setResistance(10F);
			setLightLevel(0F);
			setLightOpacity(255);
			setCreativeTab(TabAssassinWeapons.tab);
		}
	}
}
