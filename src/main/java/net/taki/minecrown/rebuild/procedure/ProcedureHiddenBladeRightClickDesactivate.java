package net.taki.minecrown.rebuild.procedure;

@ElementsAssassincrownMod.ModElement.Tag
public class ProcedureHiddenBladeRightClickDesactivate extends ElementsAssassincrownMod.ModElement {

	public ProcedureHiddenBladeRightClickDesactivate(ElementsAssassincrownMod instance) {
		super(instance, 5);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure HiddenBladeRightClickDesactivate!");
			return;
		}

		Entity entity = (Entity) dependencies.get("entity");

		if ((((entity instanceof EntityLivingBase) ? ((EntityLivingBase) entity).getHeldItemMainhand() : ItemStack.EMPTY)
				.getItem() == new ItemStack(ItemHiddenBladeA.block, (int) (1)).getItem())) {
			if (entity instanceof EntityLivingBase) {
				ItemStack _setstack = new ItemStack(ItemHiddenBladeD.block, (int) (1));
				_setstack.setCount(1);
				((EntityLivingBase) entity).setHeldItem(EnumHand.MAIN_HAND, _setstack);
				if (entity instanceof EntityPlayerMP)
					((EntityPlayerMP) entity).inventory.markDirty();
			}
		}

	}

}
