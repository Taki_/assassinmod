package net.taki.minecrown.rebuild.procedure;

import net.taki.minecrown.rebuild.gui.GuiCrownMineurGUIBroyeuse;
import net.taki.minecrown.rebuild.ElementsAssassincrownMod;
import net.taki.minecrown.rebuild.AssassincrownMod;

import net.minecraft.world.World;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.Entity;

import java.util.Map;

@ElementsAssassincrownMod.ModElement.Tag
public class ProcedureCrownMineurBroyeuseOnBlockRightClicked extends ElementsAssassincrownMod.ModElement {
	public ProcedureCrownMineurBroyeuseOnBlockRightClicked(ElementsAssassincrownMod instance) {
		super(instance, 31);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure CrownMineurBroyeuseOnBlockRightClicked!");
			return;
		}
		if (dependencies.get("x") == null) {
			System.err.println("Failed to load dependency x for procedure CrownMineurBroyeuseOnBlockRightClicked!");
			return;
		}
		if (dependencies.get("y") == null) {
			System.err.println("Failed to load dependency y for procedure CrownMineurBroyeuseOnBlockRightClicked!");
			return;
		}
		if (dependencies.get("z") == null) {
			System.err.println("Failed to load dependency z for procedure CrownMineurBroyeuseOnBlockRightClicked!");
			return;
		}
		if (dependencies.get("world") == null) {
			System.err.println("Failed to load dependency world for procedure CrownMineurBroyeuseOnBlockRightClicked!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		int x = (int) dependencies.get("x");
		int y = (int) dependencies.get("y");
		int z = (int) dependencies.get("z");
		World world = (World) dependencies.get("world");
		if (entity instanceof EntityPlayer)
			((EntityPlayer) entity).openGui(AssassincrownMod.instance, GuiCrownMineurGUIBroyeuse.GUIID, world, x, y, z);
	}
}
