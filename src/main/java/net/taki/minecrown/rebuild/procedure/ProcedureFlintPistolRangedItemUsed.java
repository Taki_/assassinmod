package net.taki.minecrown.rebuild.procedure;

@ElementsAssassincrownMod.ModElement.Tag
public class ProcedureFlintPistolRangedItemUsed extends ElementsAssassincrownMod.ModElement {

	public ProcedureFlintPistolRangedItemUsed(ElementsAssassincrownMod instance) {
		super(instance, 22);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure FlintPistolRangedItemUsed!");
			return;
		}
		if (dependencies.get("itemstack") == null) {
			System.err.println("Failed to load dependency itemstack for procedure FlintPistolRangedItemUsed!");
			return;
		}

		Entity entity = (Entity) dependencies.get("entity");
		ItemStack itemstack = (ItemStack) dependencies.get("itemstack");

		if (entity instanceof EntityPlayer)
			((EntityPlayer) entity).getCooldownTracker().setCooldown(((itemstack)).getItem(), (int) 35);

	}

}
